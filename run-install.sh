#!/bin/sh

sudo timedatectl --no-ask-password set-ntp true
sudo timedatectl --no-ask-password set-local-rtc true

sudo apt install -y software-properties-common

sudo apt-add-repository -y ppa:ansible/ansible
sudo apt update
sudo apt install -y openssh-server git ansible python-apt

ansible-galaxy install -r requirements.yml
ansible-playbook playbook.yml -i hosts --ask-become-pass
