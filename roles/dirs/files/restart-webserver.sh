#!/usr/bin/env bash

sudo systemctl restart {nginx,apache2,php7.0-fpm,php7.1-fpm,mysql}.service
