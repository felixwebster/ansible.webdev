#!/usr/bin/env bash

sudo update-alternatives --set phpize /usr/bin/phpize7.1
sudo update-alternatives --set php /usr/bin/php7.1
sudo update-alternatives --set php-config /usr/bin/php-config7.1

sudo pecl config-set php_ini /etc/php/7.1/cli/php.ini
sudo pecl config-set ext_dir /usr/lib/php/20160303/
sudo pecl config-set bin_dir /usr/bin/
sudo pecl config-set php_bin /usr/bin/php7.1
sudo pecl config-set php_suffix 7.1

sudo a2dismod php7.0
sudo a2enmod php7.1
sudo systemctl restart apache2.service
