# Что планируется доделать:
*  Установка плагинов в браузеры:
    *  Vue.js devtools (https://vuejs.org)
    *  Xdebug helper (Wrep)
*  fail2ban
*  MSSQL Driver
    *  https://docs.microsoft.com/en-us/sql/connect/php/installation-tutorial-linux-mac?view=sql-server-2017
    *  https://docs.microsoft.com/en-us/sql/connect/odbc/linux-mac/installing-the-microsoft-odbc-driver-for-sql-server?view=sql-server-2017
*  Elastic Search
    *  https://www.digitalocean.com/community/tutorials/how-to-install-java-with-apt-on-ubuntu-18-04#installing-the-oracle-jdk
    *  https://www.digitalocean.com/community/tutorials/how-to-install-elasticsearch-logstash-and-kibana-elastic-stack-on-ubuntu-18-04#step-1-%E2%80%94-installing-and-configuring-elasticsearch
*  MemCached
    *  https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-memcached-on-ubuntu-18-04
    *  https://guides.wp-bullet.com/configure-memcached-to-use-unix-socket-speed-boost/
*  Redis
*  Bitrix Push Server
