#!/usr/bin/env bash

# Куда положить бинарник
output='/usr/local/bin/docker-compose'

# Узнаём номер последней версии
compose_version=$(curl https://api.github.com/repos/docker/compose/releases/latest | jq .name -r)

# Скачиваем бинарник и кладём по нужному пути
curl -L https://github.com/docker/compose/releases/download/$compose_version/docker-compose-$(uname -s)-$(uname -m) -o $output

# Делаем исполняемым
chmod +x $output

echo $(docker-compose --version)
