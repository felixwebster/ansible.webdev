#!/usr/bin/env bash

sudo update-alternatives --set phpize /usr/bin/phpize7.0
sudo update-alternatives --set php /usr/bin/php7.0
sudo update-alternatives --set php-config /usr/bin/php-config7.0

sudo pecl config-set php_ini /etc/php/7.0/cli/php.ini
sudo pecl config-set ext_dir /usr/lib/php/20151012/
sudo pecl config-set bin_dir /usr/bin/
sudo pecl config-set php_bin /usr/bin/php7.0
sudo pecl config-set php_suffix 7.0

sudo a2dismod php7.1
sudo a2enmod php7.0
sudo systemctl restart apache2.service
