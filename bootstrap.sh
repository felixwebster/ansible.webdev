#!/bin/sh

sudo timedatectl --no-ask-password set-ntp true
sudo timedatectl --no-ask-password set-local-rtc true

sudo apt install -y software-properties-common

sudo apt-add-repository -y ppa:ansible/ansible
sudo apt update
sudo apt install -y openssh-server git ansible python-apt

git clone https://gitlab.com/felixwebster/ansible.webdev.git

ansible-galaxy install -r ansible.webdev/requirements.yml
ansible-playbook ansible.webdev/playbook.yml -i ansible.webdev/hosts --ask-become-pass
