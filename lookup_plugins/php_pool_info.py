from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

DOCUMENTATION = """
    lookup: php_pool_info
    author: Felix Antufiev <felix@oneway.su>
    short_description: Helps generate php-fpm pools settings
    description:
      - Returns an array of ini-parameters for php-fpm pools, ready for use in the ini_file module
    options:
      _terms:
        description: first - versions info, second - username
        required: True
"""

from ansible.errors import AnsibleError
from ansible.module_utils.six import string_types
from ansible.plugins.lookup import LookupBase
from ansible.utils.listify import listify_lookup_plugin_terms


class LookupModule(LookupBase):

    def _makeArray(self, versions_info, user_name):
        iniParams = [
            {"option": "user", "value": user_name},
            {"option": "group", "value": user_name},
            {"option": "listen.owner", "value": user_name},
            {"option": "listen.group", "value": user_name}
        ]

        cp1251params = [
            {"option": "php_admin_value[mbstring.func_overload]", "value": "0"},
            {"option": "php_admin_value[mbstring.internal_encoding]", "value": ""},
        ]

        result = []

        for version in versions_info:
            groupName = "www-" + version["version"] + "-" + version["encoding"]

            filePath = "/etc/php/" + version["version"] + "/fpm/pool.d/www." + version["encoding"] + ".conf"
            for iniParam in iniParams:
                result.append({
                    "file": filePath,
                    "section": groupName,
                    "option": iniParam["option"],
                    "value": iniParam["value"],
                })

            result.append({
                "file": filePath,
                "section": groupName,
                "option": "listen",
                "value": "/run/php/php" + version["version"] + "-fpm." + version["encoding"] + ".sock",
            })

            if version["encoding"] == "cp1251":
                for iniParam in cp1251params:
                    result.append({
                        "file": filePath,
                        "section": groupName,
                        "option": iniParam["option"],
                        "value": iniParam["value"],
                    })

        return result

    def run(self, terms, variables=None, **kwargs):

        return self._makeArray(terms[0], terms[1])
