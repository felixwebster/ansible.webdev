#  Конфигурация Ansible для настройки локального окружения для разработки под 1C-Bitrix

## Как начать работать

Ставим Ubuntu 18.04 и младше. Выполянем в консоли и следуем подсказкам на экране
```
bash <(wget -qO- https://gitlab.com/felixwebster/ansible.webdev/raw/master/bootstrap.sh)

или

bash <(curl -L https://gitlab.com/felixwebster/ansible.webdev/raw/master/bootstrap.sh)
```

## О настроенном

*  PHP
    *  Сконфигурированы PHP-FPM 7.0 и 7.1. По два пула под кодировки utf8 и cp1251 (Всего 4-ре пула)
    *  FPM работает от пользователя, для которого конфигурировалась система
    *  mod-apache и cli сконфигурированы под кодировку utf8
    *  Настроен xDebug. Порт = 9000, idekey = PHPSTORM. Логи складываются в ~/Xdebug
    *  Установлены глобально Composer и PHP Code Sniffer
    *  Устанавлены пакеты: cli, common, curl, dev, fpm, gd, intl, json, mbstring, mcrypt, mysql, opcache, readline, soap, xml, zip
    *  Изменяемые параметры по-умолчанию в php.ini можно посмотреть в `roles/php/tasks/configure-php-ini.yml`

*  MySQL
    *  Версия 5.7
    *  Конфигурация взята из BitrixVM 7, всё лежит в /etc/mysql/my.cnf
    *  Установлен Adminer, доступен по адресу http://adminer.local/. Лежит в ~/Projects/adminer.local

*  Apache2
    *  Слушает порт 8080
    *  Работает от пользователя, для которого конфигурировалась система
    *  Подключенные модули: proxy, proxy_fcgi, rewrite, remoteip
    *  Отключенные модули: status

*  NGINX
    *  Конфигурация взята из BitrixVM 7 и собрана в один файл bitrix.conf
    *  SSL сертификаты создаются с помощью mkcert и складываются в /etc/nginx/certificates
    *  Корневой сертификат mkcert, созданный при установке, прописан в системное хранилище и хранилища Google Chrome и Firefox
    *  Сконфигурирован дефолтный сайт. Конфигурация срабатывает, если ни одна другая не сработала. Можно посмотреть по адресу https://default.local/
    *  Работает от пользователя, для которого конфигурировалась система

*  Как создать новую площадку?
    *  Описание площадок в файле `vars/main.yml`
    *  Неочевидные переменные описаны в комментарии к файлу
    *  После редактирования файла надо запустить `run-update-hosts.sh`

*  Обработка почты
    *  Установлен MailHog и mhsendmail
    *  Доступен по адресу http://mailhog.local/
    *  Постоянное хранение почты не настроено, ппосле перезагрузки писем не останется

*  Скрипты в папке ~/Scripts
    *  restart-webserver.sh - перезагружает все веб-сервисы: nginx, apache2, mysql, php-fpm
    *  update-adminer.sh - скачивает последнюю версию Adminer
    *  update-composer.sh - выполняет composer self-update
    *  update-docker-compose.sh - скачивает последнюю версию docker-compose и кладёт в /usr/local/bin
    *  php-version-set-*.sh - меняет используемую по-умолчанию версию php

*  Файрволл
    *  Используется ufw
    *  Запрещены все входящие подключения
    *  Разрешены все исходящие подключения
    *  Разрешено входящее подключение по tcp/12122 (для SSH)

*  SSH
    *  Доступ по порту 12122
    *  Запрещена авторизация по паролю

*  Для сборки вёрстки:
    *  Установлены nodejs 10 и yarn
    *  Установлен глобально gulp через yarn

*  Утилиты
    *  CLI утилиты из deb репозиториев:
        *  ansible
        *  aptitude
        *  atop
        *  catdoc
        *  curl
        *  docker.io
        *  gdebi
        *  gdebi-core
        *  git
        *  htop
        *  jq
        *  language-pack-ru
        *  mc
        *  nmap
        *  sed
        *  snapd
        *  unzip
        *  vim
        *  wget
        *  xmllint
    *  GUI утилиты из deb репозиториев
        *  chromium-browser
        *  virtualbox
        *  firefox
        *  flameshot
        *  gimp
        *  gparted
        *  inkscape
        *  keepassxc
        *  libreoffice
        *  libreoffice-calc
        *  libreoffice-draw
        *  libreoffice-impress
        *  libreoffice-math
        *  libreoffice-writer
        *  vlc
    *  GUI утилиты из snap-пакетов
        *  telegram-desktop
        *  postman
        *  tor
    *  Остальные утилиты
        *  Google Chrome
        *  Visual Studio Code
        *  Dropbox
        *  Jetbrains Toolbox
        *  Skype
        *  Sublime Text
        *  Team Viewer
        *  Docker Compose
